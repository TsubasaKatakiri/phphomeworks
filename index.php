<?php
//Functions Task 3
echo "Functions Task 3<br>";
$number = 5;
echo "Factorial of the number $number is " . factorial($number);

function factorial(int $num)
{
    if ($num == 1) {
        return $num;
    } else {
        return $num * factorial($num - 1);
    }
}

//Fu Task 4
echo "<br><br>Functions Task 4<br>";
$number = 8;
outputNumbers($number);

function outputNumbers(int $num)
{
    if ($num >= 0) outputNumbers($num - 1);
    else return;
    echo $num;
}

?>
<html lang="en">
<head>
    <title>Task1</title>
</head>
<body>
</body>
</html>
